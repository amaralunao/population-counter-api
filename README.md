API to count population for a given area (assignment)
========================================
### API which provides an endpoint that takes as parameters a latitude, a longitude and a radius and returns the number of people living in the indicated area
---

Steps and requirements to use:

___Dependencies___:

* Project requires python3.
* The project's python dependencies are listed in the requirements.txt file and may be installed using pip. 
* Package rasterio has a dependency on GDAL >=1.11. 
  GDAL itself depends on some other libraries provided by most major operating systems and also depends on the non standard GEOS and PROJ4 libraries
  Please see rasterio docs page for proper installation instructions:<https://rasterio.readthedocs.io/en/latest/installation.html>.

___Download Raster___:

* Data source can be downloaded from: <http://sedac.ciesin.columbia.edu/data/set/gpw-v4-population-count-adjusted-to-2015-unwpp-country-totals-rev10/data-download>
(adjusted version) or from <http://sedac.ciesin.columbia.edu/data/set/gpw-v4-population-count-rev10/data-download>.
* The data files were produced as global rasters at 30 arc-second (~1 km at the equator) resolution.
* You are required to register and to login to download data or maps. 
* Temporal: choose year of interest
* FileFormat: choose GeoTiff
* Resolution: choose 30 Second (approx. 1km)
* Click on CreateDownload and you will get an archive containing a `.tif` file. 

___Upload Raster___:

* After setting up your environment and installing the dependencies, you can run from directory containing manage.py:
    1. `./manage.py migrate` to migrate the models to sqlite local database.
    2. `./manage.py createsuperuser` to be able to login to django admin. 
    3. `./manage.py runserver` which will start a local development server at <http://127.0.0.1:8000/>
* After admin login (<<http://127.0.0.1:8000/admin/>), navigate to <http://127.0.0.1:8000/admin/counter/raster/>, click on `ADD RASTER` and upload the `.tif` file from previous step.
  (Note: You need to have some patience with the upload as it takes almost a minute to process the raster)  


___Call endpoint___:

* If upload from previous step is successful you will be able to call the <http://127.0.0.1:8000/counter/> endpoint.
* Input parameters required by form: 
    1. Radius (positive integer)
    2. Longitute (float value between -180 and 180),
    3. Latitude (float between -60 and 85) 
* After posting you should get back a json response like this:
```{"total_population_per_selected_area": 17487}```
  (Note: The population sum has been rounded to an integer)

---
___Run Tests___:

* tests may be run using tox (e.g: tox -e py36) or using pytest from ./manage.py directory
