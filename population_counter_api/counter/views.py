from rest_framework.response import Response
from rest_framework.views import APIView

from counter import models, serializers


class PopulationCounterView(APIView):
    serializer_class = serializers.InputSerializer
    model = models.PopulationCounterModel

    def post(self, request, format=None):
        input_serializer = self.serializer_class(data=request.data)
        input_serializer.is_valid(raise_exception=True)
        output_serializer = serializers.OutputSerializer(
                self.model(**input_serializer.validated_data).calculate_population_sum())
        return Response(output_serializer.data)
