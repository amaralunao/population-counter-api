import os

import numpy as np
import rasterio
from django.db import models
from django.db.models.signals import post_delete
from django.dispatch import receiver
from rest_framework.exceptions import APIException


class Raster(models.Model):
    dataset = models.FileField()

    def save(self, *args, **kwargs):
        """
        Extend save method to open, read and save raster information
        so the time consuming operation happens once at the time of import
        """
        super(Raster, self).save(*args, **kwargs)
        with rasterio.open(self.dataset) as src:
            band1_population = src.read(1)  # reading the first band as an ndarray
            nodata = src.nodata

        band1_population[band1_population == nodata] = 0  # in order to avoid "OverflowError: cannot convert float infinity to integer", assigning 0 to nodata elements
        pathset = os.path.dirname(__file__)
        np.save(os.path.join(pathset, 'band1_population'), band1_population)  # saving the array to a numpy binary file format for later use as reading the first band takes the most time.
        return


@receiver(post_delete, sender=Raster)
def submission_delete(sender, instance, **kwargs):
    """
    Ensure also raster files are cleaned up when Raster object is deleted from db
    """
    instance.dataset.delete(False)


class PopulationCounterModel:

    def __init__(self, latitude, longitude, radius):
        self.latitude = latitude
        self.longitude = longitude
        self.radius = radius
        self.center_x = None
        self.center_y = None
        self.area_x = None
        self.area_y = None
        self.raster = None
        self.band1_population = None

    def _get_raster(self):
        if self.raster is None:
            latest_raster = Raster.objects.last()
            if latest_raster:
                self.raster = latest_raster.dataset
                return self.raster
            else:
                raise APIException(detail='Please upload the geotiff raster file via django admin page')
        return self.raster

    def _get_band1_population(self):
        if isinstance(self.band1_population, np.core.memmap) is False:
            pathset = os.path.dirname(__file__)
            try:
                self.band1_population = np.load(os.path.join(pathset,
                                                'band1_population.npy'), mmap_mode='r')  # Memory mapping is especially useful for accessing small fragments of large files without reading the entire file into memory, r is read only mode.
                return self.band1_population
            except (IOError, ValueError):
                raise APIException(detail='Please upload the geotiff raster file via django admin page')

        return self.band1_population

    def _convert_coordinates_to_pixels(self):
        raster = self._get_raster()
        if self.center_x is None or self.center_y is None:
            with rasterio.open(raster) as dataset:
                center_y, center_x = dataset.index(self.longitude, self.latitude)  # using index method from the rasterio data reader
                self.center_y, self.center_x = int(center_y), int(center_x)
                return self.center_y, self.center_x
        return self.center_y, self.center_x  # since the pixel coordinates are used in at least two other methods, making sure conversion happens just once

    def _validate_radius_length(self):
        """
        Preliminary validation on radius length
        """
        band1_population = self._get_band1_population()
        if self.radius >= min(band1_population.shape):
            raise APIException(detail="Area is out of range, try adjusting radius to a lower value")

    def _get_area_of_interest(self):
        """
        Create a grid of all the indices with the center determined by given pixel coordinates and 2*radius length and width
        """
        if isinstance(self.area_y, np.ndarray) is False \
                or isinstance(self.area_x, np.ndarray) is False:
            center_y, center_x = self._convert_coordinates_to_pixels()
            self.area_y, self.area_x = np.ogrid[center_y - self.radius:center_y + self.radius,
                                                center_x - self.radius:center_x + self.radius]
        return self.area_y, self.area_x

    def _create_circular_mask_for_area(self):
        """
        Create the indices (or pixel coordinates) of the area then check each pixel coordinate to see if it's inside or outside the circle.
        In order to tell whether it's inside the circle, one can simply find the Euclidean distance from the center to every pixel location
        and  if that distance is less than the circle radius, mark as included in the mask, and if it's greater than that, exclude it from the mask.
        true/false value for whether or not that pixel is within the radius
        """
        area_y, area_x = self._get_area_of_interest()
        center_y, center_x = self._convert_coordinates_to_pixels()
        mask = np.sqrt((area_x - center_x)**2 + (area_y - center_y)**2) <= self.radius
        return mask

    def calculate_population_sum(self):
        self._validate_radius_length()
        area_y, area_x = self._get_area_of_interest()
        mask = self._create_circular_mask_for_area()
        band1_population = self._get_band1_population()
        try:
            population_array = band1_population[area_y, area_x][mask]  # select the square grid and then apply the circular mask
        except IndexError:
            raise APIException(detail="Area is out of range, try adjusting radius to a lower value")
        population_sum = np.sum(population_array)  # sum up all the individual values for the total
        return {'total_population_per_selected_area': int(population_sum)}  # serializer friendly format
