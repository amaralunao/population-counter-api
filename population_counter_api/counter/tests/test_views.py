import os

import pytest
from django.test import TestCase, override_settings
from django.urls import reverse
from rest_framework.test import APIRequestFactory

from counter import models as counter_models
from counter import views


@pytest.mark.django_db
@override_settings(MEDIA_ROOT=os.path.dirname(os.path.dirname(os.path.abspath(__file__))))
class TestPopulationCounterView(TestCase):
    factory = APIRequestFactory()
    endpoint = reverse('total-population')

    def setUp(self):
        self.dir_absolut_path = os.path.dirname(__file__)
        self.raster = os.path.join(self.dir_absolut_path, 'gpw_v4_population_count_adjusted_to_2015_unwpp_country_totals_rev10_2020_30_sec.tif')
        counter_models.Raster.objects.create(dataset=self.raster)

    def test_view_total_population_success(self):
        request_data = {"radius": "20", "longitude": "4.481735", "latitude": "52.157492"}
        request = self.factory.post(self.endpoint, request_data, format='json')
        response = views.PopulationCounterView.as_view()(request)
        assert response.status_code == 200
        assert response.data['total_population_per_selected_area']

    def test_view_total_population_missing_input(self):
        request_data = {"radius": "", "longitude": "", "latitude": ""}
        request = self.factory.post(self.endpoint, request_data, format='json')
        response = views.PopulationCounterView.as_view()(request)
        assert response.status_code == 400
        assert response.data["radius"]
        assert response.data["longitude"]
        assert response.data["latitude"]

    def test_view_total_population_invalid_coord_input(self):
        request_data = {"radius": "20", "longitude": "190", "latitude": "-100"}
        request = self.factory.post(self.endpoint, request_data, format='json')
        response = views.PopulationCounterView.as_view()(request)
        assert response.status_code == 400
        assert response.data["longitude"]
        assert response.data["latitude"]

    def test_view_total_population_invalid_radius_input(self):
        request_data = {"radius": "17400", "longitude": "80", "latitude": "40"}
        request = self.factory.post(self.endpoint, request_data, format='json')
        response = views.PopulationCounterView.as_view()(request)
        assert response.status_code == 500
        assert response.data["detail"]
