import os
from unittest.mock import MagicMock, patch

import numpy as np
import pytest
from django.conf import settings
from django.test import TestCase, override_settings
from rest_framework.exceptions import APIException

from counter import models as counter_models


@pytest.mark.django_db
@override_settings(MEDIA_ROOT=os.path.dirname(os.path.dirname(os.path.abspath(__file__))))
class TestPopulationCounter(TestCase):

    def setUp(self):
        self.dir_absolut_path = os.path.dirname(__file__)
        self.raster = os.path.join(self.dir_absolut_path, 'gpw_v4_population_count_adjusted_to_2015_unwpp_country_totals_rev10_2020_30_sec.tif')
        counter_models.Raster.objects.create(dataset=self.raster)

    def test_get_raster_success(self):
        assert counter_models.Raster.objects.count() == 1
        dataset = counter_models.PopulationCounterModel(52, 4, 3)._get_raster()
        assert dataset == counter_models.Raster.objects.last().dataset

    def test_convert_coordinates_to_pixels_success(self):
        pixel_coordinates = counter_models.PopulationCounterModel(
                52.157492, 4.48173, 3)._convert_coordinates_to_pixels()
        assert pixel_coordinates == (3941, 22137)

    @patch('os.path.dirname')
    def test_get_band1_population_success(self, mock_dir):
        mock_dir.return_value = self.dir_absolut_path
        band1_population = counter_models.PopulationCounterModel(
                52.157492, 4.48173, 3)._get_band1_population()
        assert isinstance(band1_population, np.core.memmap)

    @patch('counter.models.PopulationCounterModel._convert_coordinates_to_pixels')
    def test_get_area_of_interest_success(self, mock_convert):
        mock_convert.return_value = (3941, 22137)
        area_y, area_x = counter_models.PopulationCounterModel(
                52.157492, 4.48173, 6)._get_area_of_interest()
        assert area_y.size == area_x.size  == 12
        assert area_y[6, 0] == 3941
        assert area_x[0, 6] == 22137

    @patch('counter.models.PopulationCounterModel._get_band1_population')
    def test_validate_radius_length_success(self, mock_band1):
        mock_band1.return_value = np.ndarray(shape=(10, 11), dtype=float, order='F')
        counter_models.PopulationCounterModel(22, 33, 9)._validate_radius_length()
        assert mock_band1.called_once

    @patch('counter.models.PopulationCounterModel._convert_coordinates_to_pixels')
    @patch('counter.models.PopulationCounterModel._get_area_of_interest')
    def test_create_circular_mask_for_area_success(self, mock_area, mock_convert):
        mock_area.return_value = np.ogrid[1:9, 2:10]
        mock_convert.return_value = (4, 6)
        mask = counter_models.PopulationCounterModel(4, 52, 4)._create_circular_mask_for_area()
        mask_corners = mask[[0, 0, -1, -1], [0, -1, 0, -1]]
        assert mask[4, 6]
        for x in mask_corners:
            assert not x  # since x if of type numpy.bool, weird test results can occur when assertion is explicit like: AssertionError: False is False

    @patch('counter.models.PopulationCounterModel._validate_radius_length')
    @patch('counter.models.PopulationCounterModel._get_area_of_interest')
    @patch('counter.models.PopulationCounterModel._create_circular_mask_for_area')
    @patch('counter.models.PopulationCounterModel._get_band1_population')
    def test_calculate_population_sum(self, mock_band1, mock_mask, mock_area, mock_validate):
        mock_band1.return_value = np.array([[1, 2, 3, 4, 5],
                                            [6, 7, 8, 9, 10],
                                            [11, 12, 13, 14, 15],
                                            [11, 12, 13, 14, 15],
                                            [6, 7, 8, 9, 10]], np.int32)
        mock_mask.return_value = np.array([[False, False,  True, False],
                                           [False,  True,  True,  True],
                                           [True,  True,  True,  True],
                                           [False,  True,  True, True]], np.bool)
        mock_area.return_value = np.ogrid[0:4, 0:4]
        population_output = counter_models.PopulationCounterModel(2, 2, 2).calculate_population_sum()
        assert population_output['total_population_per_selected_area'] == 116


class TestPopulationCounterExceptions(TestCase):

    def test_get_raster_raises_exception(self):
        with pytest.raises(APIException):
            counter_models.PopulationCounterModel(52, 4, 3)._get_raster()

    @patch('os.path.dirname')
    def test_get_band1_population_raises_exception(self, mock_dir):
        mock_dir.return_value = settings.BASE_DIR
        with pytest.raises(APIException):
            counter_models.PopulationCounterModel(52, 4, 3)._get_band1_population()

    @patch('counter.models.PopulationCounterModel._get_band1_population')
    def test_validate_radius_length_raises_exception(self, mock_band1):
        mock_band1 = MagicMock
        mock_band1.shape = (17400, 43200)
        with pytest.raises(APIException):
            counter_models.PopulationCounterModel(4, 52, 17400)._validate_radius_length()

    @patch('counter.models.PopulationCounterModel._validate_radius_length')
    @patch('counter.models.PopulationCounterModel._get_area_of_interest')
    @patch('counter.models.PopulationCounterModel._create_circular_mask_for_area')
    @patch('counter.models.PopulationCounterModel._get_band1_population')
    def test_calculate_population_sum_raises_exception_index_error(self, mock_band1, mock_mask, mock_area, mock_validate):
        mock_band1.return_value = np.array([[1, 2, 3],
                                            [6, 7, 8],
                                            [6, 7, 8]], np.int32)
        mock_mask.return_value = np.array([[False, False,  True, False],
                                           [False,  True,  True,  True],
                                           [True,  True,  True,  True],
                                           [False,  True,  True, True]], np.bool)
        mock_area.return_value = np.ogrid[0:4, 0:4]
        with pytest.raises(APIException):
            counter_models.PopulationCounterModel(2, 2, 2).calculate_population_sum()

    @patch('counter.models.PopulationCounterModel._validate_radius_length')
    @patch('counter.models.PopulationCounterModel._get_area_of_interest')
    @patch('counter.models.PopulationCounterModel._create_circular_mask_for_area')
    @patch('counter.models.PopulationCounterModel._get_band1_population')
    def test_calculate_population_sum_raises_exception_validation_error(self, mock_band1, mock_mask, mock_area, mock_validate):
        mock_validate.side_effect = APIException
        with pytest.raises(APIException):
            counter_models.PopulationCounterModel(2, 2, 2).calculate_population_sum()
