from rest_framework import serializers


class InputSerializer(serializers.Serializer):
    radius = serializers.IntegerField(min_value=0, required=True)
    longitude = serializers.FloatField(min_value=-180, max_value=180, required=True)
    latitude = serializers.FloatField(min_value=-60, max_value=85, required=True)  # according to the extent of the geotiff raster info


class OutputSerializer(serializers.Serializer):
    total_population_per_selected_area = serializers.IntegerField()
