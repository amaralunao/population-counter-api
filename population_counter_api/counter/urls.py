from django.urls import path

from . import views

urlpatterns = [
   path('', views.PopulationCounterView.as_view(), name='total-population'),
]
